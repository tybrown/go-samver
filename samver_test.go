package samver_test

import (
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"gitlab.com/tybrown/go-samver"
)

func Example() {
	var (
		ExampleCommitTime = time.Date(1970, 1, 1, 11, 30, 15, 0, time.UTC)
		ExampleCommitSha  = "deadbeef1234567890"
	)

	s, err := samver.New(ExampleCommitTime, ExampleCommitSha, &samver.Options{})
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println(s)
	// Output: 19700101.1130.15-deadbee
}

func Example_withDirty() {
	var (
		ExampleCommitTime = time.Date(1970, 1, 1, 11, 30, 15, 0, time.UTC)
		ExampleCommitSha  = "deadbeef1234567890"
	)

	s, err := samver.New(ExampleCommitTime, ExampleCommitSha, &samver.Options{DirtyWorkspace: true})
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println(s)
	// Output: 19700101.1130.15-deadbee-DIRTY
}

func Example_withoutShaSuffix() {
	var (
		ExampleCommitTime = time.Date(1970, 1, 1, 11, 30, 15, 0, time.UTC)
		ExampleCommitSha  = "deadbeef1234567890"
	)

	s, err := samver.New(ExampleCommitTime, ExampleCommitSha, &samver.Options{DisableShaSuffix: true})
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println(s)
	// Output: 19700101.1130.15
}

func TestSamver_withLeadingZeroHour(t *testing.T) {
	assert := assert.New(t)

	timestamp := time.Date(1970, 1, 1, 1, 0, 15, 0, time.UTC)
	s, _ := samver.New(timestamp, "deadbeef1234567890", &samver.Options{})

	assert.Equal("100", s.Minor(), "There should not be a leading 0 here")
}

func TestSamver_withExactlyMidnight(t *testing.T) {
	assert := assert.New(t)

	timestamp := time.Date(1970, 1, 1, 0, 0, 15, 0, time.UTC)
	s, _ := samver.New(timestamp, "deadbeef1234567890", &samver.Options{})

	assert.Equal("0", s.Minor(), "When HH:MM is 00:00, the value should be exactly 0")
}

func TestSamver_withNilOptions(t *testing.T) {
	assert := assert.New(t)

	s, err := samver.New(time.Now(), "deadbeef", nil)

	assert.ErrorIs(err, samver.OptionsNilError)
	assert.Nil(s)
}

func TestSamver_withLeadingZeroSha(t *testing.T) {
	assert := assert.New(t)

	timestamp := time.Date(1970, 1, 1, 11, 30, 15, 0, time.UTC)
	s, _ := samver.New(timestamp, "0abcd1234", &samver.Options{})

	assert.Equal("zabcd12", s.Suffix(), "Suffixes can't have leading 0, 0 should be replaced with a z")
}

func TestSamver_withNonUtcTimezone(t *testing.T) {
	assert := assert.New(t)

	// we'll use Chicago for our test
	location, _ := time.LoadLocation("America/Chicago")

	timestamp := time.Date(1970, 1, 1, 11, 30, 15, 0, location)
	s, _ := samver.New(timestamp, "deadbeef", &samver.Options{})

	assert.Equal("19700101.1730.15-deadbee", s.String(), "The Samver timestamp should always be converted to UTC")
}

func TestSamver_withDirtyTrueAndDisableDirty(t *testing.T) {
	assert := assert.New(t)

	timestamp := time.Date(1970, 1, 1, 11, 30, 15, 0, time.UTC)
	s, _ := samver.New(timestamp, "abcd1234", &samver.Options{DirtyWorkspace: true, DisableDirtySuffix: true})

	assert.Equal("19700101.1130.15-abcd123", s.String(),
		"When DisableDirtySuffix is true, skip printing Dirty Suffix even if DirtyWorkspace==true")
}
