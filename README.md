# samver

[![GoDoc](https://img.shields.io/badge/pkg.go.dev-doc-blue)](http://pkg.go.dev/gitlab.com/tybrown/go-samver)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/tybrown/go-samver)](https://goreportcard.com/report/gitlab.com/tybrown/go-samver)

Package samver provides formatting options for generating
[https://samver.org/](https://samver.org/) compliant version strings.

## Types

### type [Options](/options.go#L4)

`type Options struct { ... }`

Options is used to adjust the output behavior of a formatted Samver

### type [Samver](/samver.go#L31)

`type Samver struct { ... }`

Samver is the type that does all the work here! Initialize with one of the three factory methods:

```go
* New(timestamp, time.Time, commitSha string, opts *Options) (*Samver, error)
* NewFromGitRepo(opts *Options) (*Samver, error)
* NewFromGitRepoPath(path string, opts *Options) (*Samver, error)
```

#### func [Must](/helpers.go#L21)

`func Must(samver *Samver, err error) *Samver`

Must is a helper that wraps a call to a function returning (*Samver, error)
and panics if the error is non-nil. It is intended for use in variable
initializations such as

```go
var s = samver.Must(samver.New(timestamp, commitSHA, opts))
```

#### func [New](/samver.go#L45)

`func New(timestamp time.Time, commitSha string, opts *Options) (*Samver, error)`

New takes a commit timestamp, SHA, and options, parses the values, and returns a
Samver object with the correct formatting all done for you.

Use NewFromGitRepo if you want auto-detection of the commit details when invoked
from inside of a Git Repo directory.

#### func [NewFromGitRepo](/samver.go#L63)

`func NewFromGitRepo(opts *Options) (*Samver, error)`

NewFromGitRepo is like New(), but instead utilizes a Pure Go implementation of Git
([https://github.com/go-git/go-git](https://github.com/go-git/go-git)) to detect the current commit information from the
Git repo in the current directory (or parent directory).

#### func [NewFromGitRepoPath](/samver.go#L74)

`func NewFromGitRepoPath(path string, opts *Options) (*Samver, error)`

NewFromGitRepoPath is like NewFromGitRepo(), but instead accepts a path to the Git repo
from which Go-Git will read the current commit information.

#### func [NewFromGoBuildInfo](/samver.go#L110)

`func NewFromGoBuildInfo(opts *Options) (*Samver, error)`

NewFromGoBuildInfo reads Git Version information from Go 1.18 and later SCM stamped binaries.
See [https://go.dev/doc/go1.18#go-version](https://go.dev/doc/go1.18#go-version) for more information about this mechanism.

## Examples

```golang
var (
    ExampleCommitTime = time.Date(1970, 1, 1, 11, 30, 15, 0, time.UTC)
    ExampleCommitSha  = "deadbeef1234567890"
)

s, err := samver.New(ExampleCommitTime, ExampleCommitSha, &samver.Options{})
if err != nil {
    fmt.Println(err)
    return
}

fmt.Println(s)
```

 Output:

```
19700101.1130.15-deadbee
```

### WithDirty

```golang
var (
    ExampleCommitTime = time.Date(1970, 1, 1, 11, 30, 15, 0, time.UTC)
    ExampleCommitSha  = "deadbeef1234567890"
)

s, err := samver.New(ExampleCommitTime, ExampleCommitSha, &samver.Options{DirtyWorkspace: true})
if err != nil {
    fmt.Println(err)
    return
}

fmt.Println(s)
```

 Output:

```
19700101.1130.15-deadbee-DIRTY
```

### WithoutShaSuffix

```golang
var (
    ExampleCommitTime = time.Date(1970, 1, 1, 11, 30, 15, 0, time.UTC)
    ExampleCommitSha  = "deadbeef1234567890"
)

s, err := samver.New(ExampleCommitTime, ExampleCommitSha, &samver.Options{DisableShaSuffix: true})
if err != nil {
    fmt.Println(err)
    return
}

fmt.Println(s)
```

 Output:

```
19700101.1130.15
```

---
Readme created from Go doc with [goreadme](https://github.com/posener/goreadme)
