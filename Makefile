# HELP
# This will output the help for each task
# thanks to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
.PHONY: help
help: ## This help.
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)
.DEFAULT_GOAL := help

.PHONY: all
all: dist/bin/samvercli readme ## Builds all artifacts

.PHONY: readme
readme: ## Generates the README from Go Documentation strings
	goreadme -types -factories -badge-godoc -badge-goreportcard -import-path gitlab.com/tybrown/go-samver > README.md

dist/bin/samvercli: dist/bin fmt tidy test ## Builds samvercli
	go build -o ./dist/bin/samvercli ./cmd/samvercli


dist/bin: ## Creates dist/bin output olders for build bindaries
	mkdir -p ./dist/bin

.PHONY: fmt
fmt: ## go fmt against all files
	go fmt ./...

.PHONY: tidy
tidy: ## go mod tidy
	go mod tidy

.PHONY: test
test: ## go test against all packages
	go test -v ./...

.PHONY: clean
clean: ## Removes built artifacts
	rm -rfv ./dist
