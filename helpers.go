package samver

import (
	"runtime/debug"
)

func convertBuildSettingsToMap(settings []debug.BuildSetting) map[string]string {
	sMap := map[string]string{}

	for _, item := range settings {
		sMap[item.Key] = item.Value
	}

	return sMap
}

// Must is a helper that wraps a call to a function returning (*Samver, error)
// and panics if the error is non-nil. It is intended for use in variable
// initializations such as
//	var s = samver.Must(samver.New(timestamp, commitSHA, opts))
func Must(samver *Samver, err error) *Samver {
	if err != nil {
		panic(err)
	}
	return samver
}
