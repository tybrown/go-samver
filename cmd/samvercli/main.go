// CLI for generating SamVer versions from a Git Repo
package main

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/tybrown/go-samver"
)

var (
	rootCmd = &cobra.Command{
		Use:   "samvercli",
		Short: "CLI for generating SamVer versions from a Git Repo",
		Long:  "CLI for generating SamVer versions from a Git Repo",
		RunE: func(cmd *cobra.Command, args []string) error {
			opts := &samver.Options{}

			noSuffixes, _ := cmd.Flags().GetBool("no-suffixes")
			if noSuffixes {
				opts.DisableDirtySuffix = true
				opts.DisableShaSuffix = true
			}

			noDirty, _ := cmd.Flags().GetBool("no-dirty-suffix")
			if noDirty {
				opts.DisableDirtySuffix = true
			}

			noSha, _ := cmd.Flags().GetBool("no-sha-suffix")
			if noSha {
				opts.DisableShaSuffix = true
			}

			s, err := samver.NewFromGitRepo(opts)
			if err != nil {
				return err
			}

			fmt.Println(s)
			return nil
		},
	}
)

func main() {
	rootCmd.Version = samver.Must(samver.NewFromGoBuildInfo(&samver.Options{})).String()
	rootCmd.PersistentFlags().BoolP("no-suffixes", "n", false, "Disables both SHA and DIRTY suffixes")
	rootCmd.PersistentFlags().BoolP("no-dirty-suffix", "", false, "Disable DIRTY suffix if repo has modifications or untracked files")
	rootCmd.PersistentFlags().BoolP("no-sha-suffix", "", false, "Disable Commit SHA suffix")

	if err := rootCmd.Execute(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
