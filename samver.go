// Package samver provides formatting options for generating
// https://samver.org/ compliant version strings.
package samver

import (
	"fmt"
	"path/filepath"
	"runtime/debug"
	"strconv"
	"strings"
	"time"

	"github.com/go-git/go-git/v5"
	"github.com/h2non/findup"
)

const (
	majorFormat = "20060102"
	minorFormat = "1504"
	patchFormat = "5"
)

// OptionsNilError is raised when the a *Option is provided to New(), but is actually
// a nil pointer instead.
var OptionsNilError = fmt.Errorf("Provided *Options was nil, must be *Options")

// Samver is the type that does all the work here! Initialize with one of the three factory methods:
//  * New(timestamp, time.Time, commitSha string, opts *Options) (*Samver, error)
//  * NewFromGitRepo(opts *Options) (*Samver, error)
//  * NewFromGitRepoPath(path string, opts *Options) (*Samver, error)
type Samver struct {
	origTimestamp time.Time
	timestamp     time.Time
	commitSha     string
	opts          *Options
}

/*
New takes a commit timestamp, SHA, and options, parses the values, and returns a
Samver object with the correct formatting all done for you.

Use NewFromGitRepo if you want auto-detection of the commit details when invoked
from inside of a Git Repo directory.
*/
func New(timestamp time.Time, commitSha string, opts *Options) (*Samver, error) {
	if opts == nil {
		return nil, OptionsNilError
	}

	s := Samver{
		origTimestamp: timestamp,
		timestamp:     timestamp.In(time.UTC), // convert to UTC, no matter what
		commitSha:     commitSha,
		opts:          opts,
	}

	return &s, nil
}

// NewFromGitRepo is like New(), but instead utilizes a Pure Go implementation of Git
// (https://github.com/go-git/go-git) to detect the current commit information from the
// Git repo in the current directory (or parent directory).
func NewFromGitRepo(opts *Options) (*Samver, error) {
	path, err := findup.Find(".git")
	if err != nil {
		return nil, fmt.Errorf("fatal: not a git repository (or any of the parent directories): .git")
	}

	return NewFromGitRepoPath(path, opts)
}

// NewFromGitRepoPath is like NewFromGitRepo(), but instead accepts a path to the Git repo
// from which Go-Git will read the current commit information.
func NewFromGitRepoPath(path string, opts *Options) (*Samver, error) {
	repo, err := git.PlainOpen(filepath.Dir(path))
	if err != nil {
		return nil, fmt.Errorf("Unable to open git repo: %w", err)
	}

	reference, err := repo.Head()
	if err != nil {
		return nil, fmt.Errorf("Unable to get HEAD ref: %w", err)
	}

	commit, err := repo.CommitObject(reference.Hash())
	if err != nil {
		return nil, fmt.Errorf("Unable to read Commit info: %w", err)
	}

	if !opts.DisableDirtySuffix {
		worktree, err := repo.Worktree()
		if err != nil {
			return nil, fmt.Errorf("Unable to get Worktree info from Git repo: %w", err)
		}

		status, err := worktree.Status()
		if err != nil {
			return nil, fmt.Errorf("Unable to get Status from Git: %w", err)
		}

		opts.DirtyWorkspace = !status.IsClean()
	}

	s, _ := New(commit.Author.When, commit.Hash.String(), opts)
	return s, nil
}

// NewFromGoBuildInfo reads Git Version information from Go 1.18 and later SCM stamped binaries.
// See https://go.dev/doc/go1.18#go-version for more information about this mechanism.
func NewFromGoBuildInfo(opts *Options) (*Samver, error) {
	info, ok := debug.ReadBuildInfo()
	if !ok {
		return nil, fmt.Errorf("Failed to ReadBuildInfo")
	}

	infoMap := convertBuildSettingsToMap(info.Settings)

	vcs, ok := infoMap["vcs"]
	if !ok {
		return nil, fmt.Errorf("Not built with VCS information stamped")
	}

	switch vcs {
	case "git":
		timestamp, err := time.Parse("2006-01-02T15:04:05Z", infoMap["vcs.time"])
		if err != nil {
			return nil, err
		}

		commitSHA, ok := infoMap["vcs.revision"]
		if !ok && !opts.DisableShaSuffix {
			return nil, fmt.Errorf("Failed to fetch Commit SHA from debug.BuildInfo")
		}

		dirty, err := strconv.ParseBool(infoMap["vcs.modified"])
		if err != nil {
			if !opts.DisableDirtySuffix {
				return nil, fmt.Errorf("Failed to determine if workspace is modified/dirty: %w", err)
			}
			dirty = false
		}
		opts.DirtyWorkspace = dirty

		return New(timestamp, commitSHA, opts)

	default:
		return nil, fmt.Errorf("VCS type not supported by samver")
	}
}

// String returns the completed Samver, based on the options and values provided
func (s Samver) String() string {
	major, minor, patch := s.TimestampElements()
	suffix := s.Suffix()

	v := fmt.Sprintf("%s.%s.%s", major, minor, patch)
	if suffix != "" {
		v = fmt.Sprintf("%s-%s", v, suffix)
	}
	return v
}

// TimestampElements returns only the MAJOR.MINOR.PATCH portion of the Samver
// without the -SUFFIX portion
func (s *Samver) TimestampElements() (major, minor, patch string) {
	return s.Major(), s.Minor(), s.Patch()
}

// Major returns only the MAJOR part of a MAJOR.MINOR.PATCH-SUFFIX version
func (s *Samver) Major() string {
	return s.timestamp.Format(majorFormat)
}

// Minor returns only the MINOR part of a MAJOR.MINOR.PATCH-SUFFIX version
func (s *Samver) Minor() string {
	v := s.timestamp.Format(minorFormat)

	// manually strip leading 0 from hours 00 through 09
	v = strings.TrimLeft(v, "0")

	// handle special case of a commit at exactly 00:00
	if v == "" {
		v = "0"
	}

	return v
}

// Patch returns only the PATCH part of a MAJOR.MINOR.PATCH-SUFFIX version
func (s *Samver) Patch() string {
	return s.timestamp.Format(patchFormat)
}

// Suffix returns the SUFFIX part of a MAJOR.MINOR.PATCH-SUFFIX version
// based on the Options that were provided earlier, that is, if DisableShaSuffix was
// true, then the SUFFIX value here may be an empty string
func (s *Samver) Suffix() string {
	v := ""

	shaShort := []byte(s.commitSha[0:7])

	var zero byte = '0'
	var z byte = 'z'
	if shaShort[0] == zero {
		shaShort[0] = z
	}

	if !s.opts.DisableShaSuffix {
		v = string(shaShort)
	}

	if !s.opts.DisableDirtySuffix && s.opts.DirtyWorkspace && v == "" {
		v = "DIRTY"
	} else if !s.opts.DisableDirtySuffix && s.opts.DirtyWorkspace {
		v = fmt.Sprintf("%s-DIRTY", v)
	}

	return v
}
