package samver

// Options is used to adjust the output behavior of a formatted Samver
type Options struct {
	// If DirtyWorkspace is true, Samver will suffix the version string with -DIRTY
	// DisableDirtySuffix must also be false
	DirtyWorkspace bool

	// If true, disables the -DIRTY suffix, even if DirtyWorkspace is true or detected
	// to be true in NewFromGitRepo()
	DisableDirtySuffix bool

	// If true, disables the Commit SHA suffix from being appended to the version string
	DisableShaSuffix bool
}
